import sys
from datetime import datetime
import random
import webbrowser

def main():
    
    hours = int(sys.argv[1])
    minutes = int(sys.argv[2])
    txtfile = sys.argv[3]
    with open(txtfile) as f:
        content = f.readlines()
    url = random.choice(content)
    alarm = False
        
    wrongHours   = isinstance(hours,  int)
    wrongMinutes   = isinstance(minutes,  int) 
    if wrongHours == False:
            print("Usage: hours,  minutes,  file")
            sys.quit(0)
            
    if wrongMinutes == False:
        print("Usage: hours,  minutes,  file")
        sys.quit(0)
        
    while alarm == False:
        if datetime.now().hour == hours:
            if datetime.now().minute == minutes:
                webbrowser.open(url,  new=1,  autoraise=True)
                alarm = True

if __name__ == '__main__':
    main()
